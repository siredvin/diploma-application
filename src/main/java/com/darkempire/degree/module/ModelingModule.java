package com.darkempire.degree.module;

import com.darkempire.anji.log.Log;
import com.darkempire.anji.util.ParamAction;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.degree.Main;
import com.darkempire.degree.SettingClass;
import com.darkempire.degree.task.MeanModelingTask;
import com.darkempire.degree.task.MultiSimulationTask;
import com.darkempire.degree.task.SimulationTask;
import com.darkempire.math.MathMachine;
import com.darkempire.math.random.linearrangomgenerators.MersenneTwisterFastGenerator;
import com.darkempire.math.struct.function.FGenerateDouble;
import com.darkempire.math.struct.probability.Distribution;
import com.darkempire.math.struct.probability.IDistribution;
import com.darkempire.math.utils.ArrayUtils;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import org.controlsfx.control.TaskProgressView;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;


import static com.darkempire.degree.SettingClass.claimsDistributionImpl;
import static com.darkempire.degree.SettingClass.incomeDistributionImpl;

/**
 * Created by siredvin on 23.04.15.
 *
 * @author siredvin
 */
public class ModelingModule {
    private List<ParamAction<ExpResult>> actionList;
    private volatile ExpResult currentResult;
    private AtomicInteger finishCount;
    private int threadRunCount;
    private LocalTime startTime, endTime;
    private boolean showMessage;
    private ExecutorService executorService;

    public ModelingModule() {
        finishCount = new AtomicInteger();
        actionList = new LinkedList<>();
        showMessage = true;
        this.executorService = Executors.newCachedThreadPool();
    }

    public List<FGenerateDouble> generateGeneratorList(int count, Random random, IDistribution distribution) {
        List<FGenerateDouble> list = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            list.add(distribution.getRandomGenerator(new MersenneTwisterFastGenerator(random.nextLong())));
        }
        return list;
    }

    public void runModeling(TaskProgressView taskProgressView, Random random) {
        runModeling(SettingClass.length, SettingClass.count, SettingClass.thread_count, SettingClass.start_cash
                , SettingClass.coef_c, taskProgressView, random);
    }

    /**
     * @param length      довжина кроків в одному експерименті
     * @param count       кількість експериментів
     * @param threadCount кількість потоків моделювання
     */
    public void runModeling(int length, int count, int threadCount, double startCash, double actuarPaiment, TaskProgressView taskProgressView, Random random) {
        int thread_run = count / threadCount;
        int thread_delta = count - thread_run * threadCount;
        int usesThreadCount = threadCount + (thread_delta > 0 ? 1 : 0);
        List<FGenerateDouble> claimsList = generateGeneratorList(usesThreadCount, random, claimsDistributionImpl);
        List<FGenerateDouble> actionList = generateGeneratorList(usesThreadCount, random, incomeDistributionImpl);
        runModeling(length, count, threadCount, startCash, actuarPaiment, taskProgressView, claimsList, actionList);
    }

    /**
     * @param length      довжина кроків в одному експерименті
     * @param count       кількість експериментів
     * @param threadCount кількість потоків моделювання
     */
    public void runModeling(int length, int count, int threadCount, double startCash, double actuarPaiment, TaskProgressView taskProgressView, List<FGenerateDouble> claimsGenerators, List<FGenerateDouble> actionGenerators) {
        finishCount.set(0);
        //Кількість потоків
        int thread_run = count / threadCount;
        int thread_delta = count - thread_run * threadCount;
        List<SimulationTask> taskList = new ArrayList<>();
        currentResult = new ExpResult();
        currentResult.count = count;
        currentResult.lenght = length;
        int delta_present = thread_delta > 0 ? 1 : 0;
        currentResult.failCounts = new int[threadCount + delta_present];
        currentResult.runCounts = new int[threadCount + delta_present];
        if (claimsDistributionImpl.getType() == Distribution.EXPONENTIAL) {
            double t_lambda = SettingClass.incomeLambda;
            double x_lambda = claimsDistributionImpl.getAllParameters()[0];
            currentResult.trueResult = (t_lambda * Math.exp(-(x_lambda - t_lambda / actuarPaiment) * startCash)) / (x_lambda * actuarPaiment);
        } else {
            currentResult.trueResult = 0;
        }
        for (byte i = 0; i < threadCount; i++) {
            taskList.add(new SimulationTask(claimsGenerators.get(i), actionGenerators.get(i),
                    startCash, actuarPaiment, length, thread_run, i));
            currentResult.runCounts[i] = thread_run;
        }
        if (thread_delta > 0) {
            taskList.add(new SimulationTask(claimsGenerators.get(threadCount), actionGenerators.get(threadCount),
                    startCash, actuarPaiment, length, thread_delta, (byte) threadCount));
            currentResult.runCounts[thread_delta] = thread_delta;
        }
        threadRunCount = taskList.size();
        startTime = LocalTime.now();
        for (SimulationTask task : taskList) {
            task.setOnSucceeded(this::handle);
            if (taskProgressView != null) {
                taskProgressView.getTasks().add(task);
            }
            executorService.execute(task);
        }
    }


    /**
     * Блокуюче моделювання. Багатопоточне, але основний потік блокується до того моменту, поки обчислення не скінчяться
     *
     * @param length           кількість кроків в одному експерименті
     * @param count            кількість експериментів
     * @param threadCount      кількість потоків
     * @param startCash        початковий капітал
     * @param actuarPaiment    страхові виплати
     * @param taskProgressView GUI компонент для відображення прогресу
     * @param random           генератор випадкових чисел для генерації інших генераторів
     * @return ймовірність банкрутства
     */
    public double blockRunModeling(int length, int count, int threadCount, double startCash, double actuarPaiment, TaskProgressView taskProgressView, Random random) {
        int thread_run = count / threadCount;
        int thread_delta = count - thread_run * threadCount;
        int usesThreadCount = threadCount + (thread_delta > 0 ? 1 : 0);
        List<FGenerateDouble> claimsGenerators = generateGeneratorList(usesThreadCount, random, claimsDistributionImpl);
        List<FGenerateDouble> actionGenerators = generateGeneratorList(usesThreadCount, random, incomeDistributionImpl);
        //Кількість потоків
        List<SimulationTask> taskList = new ArrayList<>();
        currentResult = new ExpResult();
        currentResult.count = count;
        currentResult.lenght = length;
        int delta_present = thread_delta > 0 ? 1 : 0;
        currentResult.failCounts = new int[threadCount + delta_present];
        currentResult.runCounts = new int[threadCount + delta_present];
        if (claimsDistributionImpl.getType() == Distribution.EXPONENTIAL) {
            double t_lambda = SettingClass.incomeLambda;
            double x_lambda = claimsDistributionImpl.getAllParameters()[0];
            currentResult.trueResult = (t_lambda * Math.exp(-(x_lambda - t_lambda / actuarPaiment) * startCash)) / (x_lambda * actuarPaiment);
        } else {
            currentResult.trueResult = 0;
        }
        for (byte i = 0; i < threadCount; i++) {
            taskList.add(new SimulationTask(claimsGenerators.get(i), actionGenerators.get(i),
                    startCash, actuarPaiment, length, thread_run, i));
            currentResult.runCounts[i] = thread_run;
        }
        if (thread_delta > 0) {
            taskList.add(new SimulationTask(claimsGenerators.get(threadCount), actionGenerators.get(threadCount),
                    startCash, actuarPaiment, length, thread_delta, (byte) threadCount));
            currentResult.runCounts[thread_delta] = thread_delta;
        }
        threadRunCount = taskList.size();
        if (taskProgressView != null) {
            Platform.runLater(() -> taskProgressView.getTasks().addAll(taskList));
        }
        taskList.forEach(executorService::submit);
        try {
            for (SimulationTask task : taskList) {
                byte number = task.getNumber();
                int failCount = task.get();
                currentResult.failCounts[number] = failCount;
            }
            currentResult.calcResult();
            actionList.forEach(t -> t.action(currentResult));
        } catch (InterruptedException | ExecutionException e) {
            Log.error(Main.Log_index, e);
        }
        return currentResult.result;
    }

    /**
     * Блокуюче отримання данних
     * Відміність від моделювання полягає в тому, що збирається не ймовірність, а мінімальний пік. Тобто, найменша точка
     * траекторії
     *
     * @param length           кількість кроків в одному експерименті
     * @param count            кількість експериментів
     * @param threadCount      кількість потоків
     * @param actuarPaiment    страхові виплати
     * @param taskProgressView GUI компонент для відображення прогресу
     * @param random           генератор випадкових чисел для генерації інших генераторів
     * @return масив найменших значень кожної траекторії
     */
    public double[] blockCollectData(int length, int count, int threadCount, double actuarPaiment, TaskProgressView taskProgressView, Random random) {
        int thread_run = count / threadCount;
        int thread_delta = count - thread_run * threadCount;
        int usesThreadCount = threadCount + (thread_delta > 0 ? 1 : 0);
        List<FGenerateDouble> claimsGenerators = generateGeneratorList(usesThreadCount, random, claimsDistributionImpl);
        List<FGenerateDouble> actionGenerators = generateGeneratorList(usesThreadCount, random, incomeDistributionImpl);
        //Кількість потоків
        List<MultiSimulationTask> taskList = new ArrayList<>();
        for (byte i = 0; i < threadCount; i++) {
            taskList.add(new MultiSimulationTask(claimsGenerators.get(i), actionGenerators.get(i),
                    actuarPaiment, length, thread_run, i));
        }
        if (thread_delta > 0) {
            taskList.add(new MultiSimulationTask(claimsGenerators.get(threadCount), actionGenerators.get(threadCount),
                    actuarPaiment, length, thread_delta, (byte) threadCount));
        }
        threadRunCount = taskList.size();
        if (taskProgressView != null) {
            Platform.runLater(() -> taskProgressView.getTasks().addAll(taskList));
        }
        taskList.forEach(executorService::submit);
        List<double[]> arraysList = new ArrayList<>(threadRunCount);
        try {
            for (MultiSimulationTask task : taskList) {
                double[] localData = task.get();
                arraysList.add(localData);
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.error(Main.Log_index, e);
        }
        return ArrayUtils.append(arraysList);
    }


    public double blockMeanModeling(int length, int count, int threadCount, double startCash, double actuarPaiment, TaskProgressView taskProgressView, Random random) {
        int thread_run = count / threadCount;
        int thread_delta = count - thread_run * threadCount;
        int usesThreadCount = threadCount + (thread_delta > 0 ? 1 : 0);
        List<FGenerateDouble> claimsGenerators = generateGeneratorList(usesThreadCount, random, claimsDistributionImpl);
        List<FGenerateDouble> actionGenerators = generateGeneratorList(usesThreadCount, random, incomeDistributionImpl);
        //Кількість потоків
        List<MeanModelingTask> taskList = new ArrayList<>();
        for (byte i = 0; i < threadCount; i++) {
            taskList.add(new MeanModelingTask(claimsGenerators.get(i), actionGenerators.get(i), startCash,
                    actuarPaiment, length, thread_run, i, SettingClass.cutMethod));
        }
        if (thread_delta > 0) {
            taskList.add(new MeanModelingTask(claimsGenerators.get(threadCount), actionGenerators.get(threadCount), startCash,
                    actuarPaiment, length, thread_delta, (byte) threadCount, SettingClass.cutMethod));
        }
        threadRunCount = taskList.size();
        if (taskProgressView != null) {
            Platform.runLater(() -> taskProgressView.getTasks().addAll(taskList));
        }
        taskList.forEach(executorService::submit);
        double mean = 0;
        int escapeCount = 0;
        try {
            for (MeanModelingTask task : taskList) {
                double[] result = task.get();
                escapeCount += task.getEscapeNumber();
                switch (SettingClass.cutMethod) {
                    case IGNORE:
                        //mean += DoubleStream.of(result).filter(i->i>-1).sum()/ ((double) count);
                        mean += DoubleStream.of(result).filter(i -> i > -1).average().getAsDouble();
                        break;
                    case ADD_MAX:
                        //mean += DoubleStream.of(result).sum()/ ((double) count);
                        mean += DoubleStream.of(result).average().getAsDouble();
                        break;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.error(Main.Log_index, e);
        }
        double escapePercent = ((double) escapeCount) / count;
        if (escapePercent > MeanModelingTask.GOOD_ESCAPE_PERCENT) {
            Platform.runLater(() -> MonologGeneratorPane.showErrorDialog("Моделювання пройшло з величезною помилкою", "Коефіцієнт втрат:" + MathMachine.format(escapePercent)));
        }
        mean /= threadRunCount;
        return mean;
    }

    public void addOnFinishAction(ParamAction<ExpResult> action) {
        actionList.add(action);
    }

    public void clearOnFinishAction() {
        actionList.clear();
    }

    public void removeOnFinishAction(ParamAction<ExpResult> action) {
        actionList.remove(action);
    }

    private void handle(WorkerStateEvent event) {
        SimulationTask worker = (SimulationTask) event.getSource();
        byte number = worker.getNumber();
        int failCount = worker.getValue();
        currentResult.failCounts[number] = failCount;
        if (finishCount.incrementAndGet() == threadRunCount) {
            currentResult.calcResult();
            endTime = LocalTime.now();
            if (showMessage) {
                Duration duration = Duration.between(startTime, endTime);
                LocalTime localTime = LocalTime.ofNanoOfDay(duration.getNano());
                MonologGeneratorPane.showInfoDialog("Результат тесту", "Час обчислення " + localTime.format(DateTimeFormatter.ISO_LOCAL_TIME),
                        "Правильний результат:" + MathMachine.format(currentResult.trueResult) + "\nНаш результат:" + MathMachine.format(currentResult.result));
            }
            actionList.forEach(t -> t.action(currentResult));
        }
    }

    public boolean isCalculated() {
        return currentResult.isCalculated();
    }

    public double getResult() {
        if (isCalculated()) {
            return currentResult.result;
        }
        return 0;
    }

    public boolean isShowMessage() {
        return showMessage;
    }

    public void setShowMessage(boolean showMessage) {
        this.showMessage = showMessage;
    }


    public static class ExpResult {
        public volatile int[] failCounts;
        public volatile int[] runCounts;
        public int count;
        public int lenght;
        public double result;
        public double trueResult;

        public ExpResult() {
            result = trueResult = 0;
        }

        public synchronized boolean isCalculated() {
            return result != 0;
        }

        public synchronized void calcResult() {
            result = Arrays.stream(failCounts).sum() / ((double) count);
        }
    }
}
