package com.darkempire.degree.module;

import com.darkempire.anji.util.Action;
import com.darkempire.anji.util.ParamAction;
import com.darkempire.math.random.linearrangomgenerators.MersenneTwisterFastGenerator;
import javafx.scene.chart.XYChart;
import org.controlsfx.control.TaskProgressView;

import java.util.List;
import java.util.Random;

/**
 * Created by siredvin on 23.04.15.
 *
 * @author siredvin
 */
public class AnalyzeModule {
    private int constantVariable;
    private int startChange;
    private int initialChangeValue;
    private int modelingCount;
    private int endChange;
    private int threadCount;
    private int step;
    private double startCash;
    private double actuarPaiment;
    private boolean changeLength;
    private List<XYChart.Series<Double, Double>> series;
    private XYChart.Series<Double, Double> currentSeries;
    private ModelingModule modelingModule;
    private Action finishAction;
    private TaskProgressView taskView;
    private ParamAction<ModelingModule.ExpResult> analyzeResult;

    public AnalyzeModule(boolean changeLength, int constantVariable, int startChange, int endLength, int step, int threadCount, double startCash, double actuarPaiment,
                         List<XYChart.Series<Double, Double>> series, ModelingModule modelingModule, TaskProgressView taskView, int modelingCount) {
        this.constantVariable = constantVariable;
        this.startChange = initialChangeValue = startChange;
        this.endChange = endLength;
        this.step = step;
        this.changeLength = changeLength;
        this.threadCount = threadCount;
        this.startCash = startCash;
        this.actuarPaiment = actuarPaiment;
        this.series = series;
        this.modelingModule = modelingModule;
        this.taskView = taskView;
        this.modelingCount = modelingCount;
        currentSeries = series.get(0);
    }

    public void setFinishAction(Action action) {
        this.finishAction = action;
    }

    public void start() {
        analyzeResult = result -> {
            int value = changeLength ? result.lenght : result.count;
            currentSeries.getData().add(new XYChart.Data<>((double) value, Math.abs(result.result - result.trueResult)));
            next();
        };
        modelingModule.addOnFinishAction(analyzeResult);
        next();
    }

    public void next() {
        if (startChange > endChange) {
            if (modelingCount == 1) {
                modelingModule.removeOnFinishAction(analyzeResult);
                finishAction.action();
                return;
            } else {
                startChange = initialChangeValue;
                modelingCount--;
                currentSeries = series.get(series.size() - modelingCount);
            }
        }
        Random rand = new MersenneTwisterFastGenerator();
        if (changeLength) {
            modelingModule.runModeling(startChange, constantVariable, threadCount, startCash, actuarPaiment, taskView, rand);
        } else {
            modelingModule.runModeling(constantVariable, startChange, threadCount, startCash, actuarPaiment, taskView, rand);
        }
        startChange += step;
    }
}
