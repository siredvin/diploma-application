package com.darkempire.degree.module;

import com.darkempire.anji.log.Log;
import com.darkempire.anji.util.Pair;
import com.darkempire.anji.util.SerializeUtil;
import com.darkempire.anji.util.Twain;
import com.darkempire.anjifx.beans.property.AnjiDoubleProperty;
import com.darkempire.anjifx.beans.value.AnjiDoubleValue;
import com.darkempire.anjifx.dialog.DialogUtil;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.degree.Main;
import com.darkempire.math.random.linearrangomgenerators.MersenneTwisterFastGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by siredvin on 28.04.15.
 *
 * @author siredvin
 */
public class RandomModule {
    private List<Random> randomList;
    private int listIndex;
    private double eps;

    public RandomModule() {
        randomList = null;
    }

    public Random getRandom() {
        Random rand;
        if (randomList == null || randomList.size() == 0) {
            rand = new MersenneTwisterFastGenerator();
        } else {
            if (randomList.size() == listIndex) {
                listIndex = 0;
            }
            rand = SerializeUtil.clone(randomList.get(listIndex));
            listIndex++;
        }
        return rand;
    }

    public void searchRandomGenerators() {
        AnjiDoubleProperty epsProperty = new AnjiDoubleProperty("Точність", 1e-4);
        if (DialogUtil.createDialog("Налаштування зовнішньої рандомізації", epsProperty).showEdit()) {
            searchRandomGenerators(epsProperty.get());
            if (randomList.size() != 0) {
                MonologGeneratorPane.showAcceptDialog("Випадкові генератори завантажено", "Завантажено " + randomList.size() + " штук");
            }
        }
    }

    public void searchRandomGenerators(double eps) {
        if (randomList == null) {
            randomList = new ArrayList<>();
        }
        this.eps = eps;
        try {
            randomList = load();
        } catch (IOException | ClassNotFoundException e) {
            Log.error(Main.Log_index, e);
        }
    }

    public int getSize() {
        if (randomList == null) {
            return 0;
        }
        return randomList.size();
    }

    public void clear() {
        if (randomList != null) {
            randomList.clear();
        }
    }

    public double getEps() {
        return eps;
    }

    private List<Random> load() throws IOException, ClassNotFoundException {
        File sampleDir = new File("./samples/");
        File[] infoList = sampleDir.listFiles((dir, name) -> name.startsWith("info"));
        List<Random> randomList = new ArrayList<>();
        //Log.log(Log.debugIndex,eps);
        if (infoList != null) {
            for (File infoFile : infoList) {
                File objectFile = new File(infoFile.getAbsolutePath().replace("info", "object"));
                Scanner infoIn = new Scanner(new FileInputStream(infoFile));
                FileInputStream objectFileIn = new FileInputStream(objectFile);
                ObjectInputStream objectIn = new ObjectInputStream(objectFileIn);
                while (objectFileIn.available() != 0) {
                    Random random = (Random) objectIn.readObject();
                    String s = infoIn.nextLine();
                    double currentEps = Double.valueOf(s.split(":")[1]);
                    if (currentEps <= eps) {
                        randomList.add(random);
                    }
                }
                objectIn.close();
                infoIn.close();
            }
        }
        return randomList;
    }
}
