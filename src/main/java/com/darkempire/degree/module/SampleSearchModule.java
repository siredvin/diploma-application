package com.darkempire.degree.module;

import com.darkempire.anji.log.Log;
import com.darkempire.anji.util.Action;
import com.darkempire.anji.util.ParamAction;
import com.darkempire.anji.util.SerializeUtil;
import com.darkempire.degree.Main;
import com.darkempire.math.random.linearrangomgenerators.MersenneTwisterFastGenerator;
import javafx.scene.chart.XYChart;
import org.controlsfx.control.TaskProgressView;

import java.io.*;
import java.nio.file.Files;
import java.util.Random;

/**
 * Created by siredvin on 24.04.15.
 *
 * @author siredvin
 */
public class SampleSearchModule {

    private double startCash;
    private double actuarPaiment;
    private int length;
    private int count;
    private ModelingModule modelingModule;
    private XYChart.Series series;
    private ParamAction<ModelingModule.ExpResult> action;
    private int threadCount;
    private Action finishAction;

    private double eps;
    private int interateCount;
    private int findCount;


    private TaskProgressView taskProgressView;
    private ObjectOutputStream objectOut;
    private PrintWriter infoOut;
    private Random random;
    private int counter;

    public SampleSearchModule(double startCash, double actuarPaiment, int length, int count, int threadCount, int iterateCount,
                              ModelingModule modelingModule, XYChart.Series series, double eps, TaskProgressView taskProgressView) {
        this.series = series;
        this.startCash = startCash;
        this.actuarPaiment = actuarPaiment;
        this.length = length;
        this.count = count;
        this.taskProgressView = taskProgressView;
        this.threadCount = threadCount;
        this.eps = eps;
        this.interateCount = iterateCount;
        this.modelingModule = modelingModule;
        findCount = 0;
        File directory = new File("./samples");
        int number = 1;
        File objectFile = null, infoFile = null;
        while (objectFile == null) {
            objectFile = new File(directory, "object." + number + ".rsf");
            infoFile = new File(directory, "info." + number + ".rsf");
            if (Files.exists(objectFile.toPath())) {
                objectFile = null;
                number++;
            }
        }
        try {
            objectOut = new ObjectOutputStream(new FileOutputStream(objectFile));
            infoOut = new PrintWriter(new FileOutputStream(infoFile));
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
    }


    public Action getFinishAction() {
        return finishAction;
    }

    public void setFinishAction(Action finishAction) {
        this.finishAction = finishAction;
    }

    public void start() {
        action = result -> {
            try {
                double eps = Math.abs(result.trueResult - result.result);
                series.getData().add(new XYChart.Data<>(counter, eps));
                if (this.eps > eps) {
                    objectOut.writeObject(random);
                    infoOut.println("Результат:" + eps);
                    findCount++;
                }
                next();
            } catch (IOException e) {
                Log.error(Main.Log_index, e);
            }
        };
        modelingModule.addOnFinishAction(action);
        next();
    }

    public void next() {
        if (interateCount == counter) {
            try {
                objectOut.close();
                infoOut.close();
            } catch (IOException e) {
                Log.error(Main.Log_index, e);
            }
            modelingModule.removeOnFinishAction(action);
            finishAction.action();
            return;
        }
        counter++;
        Random random = new MersenneTwisterFastGenerator();
        this.random = SerializeUtil.clone(random);
        modelingModule.runModeling(length, count, threadCount, startCash, actuarPaiment, taskProgressView, random);
    }

    public int getFindCount() {
        return findCount;
    }
}
