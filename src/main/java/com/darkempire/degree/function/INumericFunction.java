package com.darkempire.degree.function;

import com.darkempire.math.struct.function.interfaces.FDoubleToDouble;

/**
 * Created by siredvin on 11.05.15.
 *
 * @author siredvin
 */
public interface INumericFunction extends FDoubleToDouble {
    public void saveFunction();
}
