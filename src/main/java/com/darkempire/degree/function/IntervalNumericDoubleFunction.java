package com.darkempire.degree.function;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.Main;
import com.darkempire.degree.module.ModelingModule;
import com.darkempire.degree.SettingClass;
import org.controlsfx.control.TaskProgressView;

import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.stream.DoubleStream;

/**
 * Created by siredvin on 11.05.15.
 *
 * @author siredvin
 */
public class IntervalNumericDoubleFunction implements INumericFunction {
    private double[] data;
    private double[] params;

    private IntervalNumericDoubleFunction(double[] params, double[] data) {
        this.data = data;
        this.params = params;
    }


    @Override
    public void saveFunction() {
        File f = IntervalNumericDoubleFunctionController.searchBind(params);
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(f));
            for (double t : data) {
                dataOutputStream.writeDouble(t);
            }
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
    }

    public double[] getData() {
        return data;
    }

    @Override
    public double calc(double x) {
        int searchIndex = Arrays.binarySearch(data, -x);
        if (searchIndex < 0) {
            searchIndex = -searchIndex - 1;
        }
        return 1 - ((double) data.length - searchIndex) / data.length;
    }


    public static IntervalNumericDoubleFunction initFunction(double[] params, ModelingModule modelingModule, TaskProgressView taskProgressView) {
        double[] data = modelingModule.blockCollectData(SettingClass.length, SettingClass.count, SettingClass.thread_count, SettingClass.coef_c, taskProgressView, SettingClass.getRandom());
        Arrays.sort(data);
        IntervalNumericDoubleFunction function = new IntervalNumericDoubleFunction(params, data);
        function.saveFunction();
        return function;
    }

    public static IntervalNumericDoubleFunction loadFunction(double[] params, ModelingModule modelingModule, TaskProgressView taskProgressView) {
        File f = IntervalNumericDoubleFunctionController.searchBind(params);
        if (!Files.exists(f.toPath())) {
            return initFunction(params, modelingModule, taskProgressView);
        }
        IntervalNumericDoubleFunction function = null;
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(f));
            int length = dataInputStream.available() / 8;
            double[] data = new double[length];
            for (int i = 0; i < length; i++) {
                data[i] = dataInputStream.readDouble();
            }
            Arrays.sort(data);
            function = new IntervalNumericDoubleFunction(params, data);
        } catch (java.io.IOException e) {
            Log.error(Main.Log_index, e);
        }
        return function;
    }
}
