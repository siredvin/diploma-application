package com.darkempire.degree.function;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.Main;

import java.io.*;
import java.nio.file.Files;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by siredvin on 29.04.15.
 *
 * @author siredvin
 */
public class CacheNumericDoubleFunctionData {
    private File dataFile;
    private Map<Double, Double> dataMap;

    public CacheNumericDoubleFunctionData(File dataFile) {
        this.dataFile = dataFile;
        dataMap = new Hashtable<>();
        loadFile();
    }

    public boolean contain(double key) {
        return dataMap.containsKey(key);
    }

    public void put(double key, double value) {
        dataMap.put(key, value);
    }

    public double get(double key) {
        Double value = dataMap.get(key);
        if (value == null) {
            Log.err(Main.Log_index, "Якась дивна помилка, повертаємо null з карти значень");
            value = Double.MIN_VALUE;
        }
        return value;
    }

    private void loadFile() {
        if (Files.exists(dataFile.toPath())) {
            try {
                DataInputStream in = new DataInputStream(new FileInputStream(dataFile));
                while (in.available() != 0) {
                    dataMap.put(in.readDouble(), in.readDouble());
                }
                in.close();
            } catch (IOException e) {
                Log.error(Main.Log_index, e);
            }
        }
    }

    public void saveFile() {
        try {
            DataOutputStream out = new DataOutputStream(new FileOutputStream(dataFile));
            for (Double key : dataMap.keySet()) {
                out.writeDouble(key);
                out.writeDouble(dataMap.get(key));
            }
            out.close();
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
    }
}
