package com.darkempire.degree.function;

import com.darkempire.anji.log.Log;
import com.darkempire.anji.util.DoubleParameters;
import com.darkempire.degree.Main;
import com.darkempire.math.utils.ArrayUtils;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by siredvin on 11.05.15.
 *
 * @author siredvin
 */
public class IntervalNumericDoubleFunctionController {
    private static Map<DoubleParameters, File> bindMap;

    static {
        init();
    }

    public static void init() {
        File bindFile = new File("./interval_data.bff");
        bindMap = new HashMap<>();
        if (Files.exists(bindFile.toPath())) {
            try {
                Scanner in = new Scanner(new FileInputStream(bindFile));
                if (in.hasNextLine()) {
                    String s = in.nextLine();
                    while (in.hasNextLine()) {
                        double[] key = ArrayUtils.valueOf(s);
                        s = in.nextLine();
                        File f = new File(s);
                        s = in.nextLine();
                        bindMap.put(new DoubleParameters(key), f);
                    }
                }
            } catch (FileNotFoundException e) {
                Log.error(Main.Log_index, e);
            }
        }
    }

    public static File searchBind(double[] params) {
        DoubleParameters key = new DoubleParameters(params);
        File f = bindMap.get(key);
        if (f == null) {
            Random random = new Random();
            f = new File("function/file." + Math.abs(random.nextInt()) + ".isv");
            while (Files.exists(f.toPath())) {
                f = new File("function/file." + Math.abs(random.nextInt(0)) + ".isv");
            }
            bindMap.put(key, f);
            dump();
        }
        return f;
    }


    public static void dump() {
        try {
            File bindFile = new File("./interval_data.bff");
            PrintWriter pw = new PrintWriter(new FileOutputStream(bindFile));
            if (bindMap != null) {
                for (DoubleParameters key : bindMap.keySet()) {
                    File value = bindMap.get(key);
                    pw.println(ArrayUtils.dumpArray(key.toRaw()));
                    pw.println(value.getAbsolutePath());
                }
            }
            pw.print("EndOfFile");
            pw.close();
        } catch (FileNotFoundException e) {
            Log.error(Main.Log_index, e);
        }
    }
}
