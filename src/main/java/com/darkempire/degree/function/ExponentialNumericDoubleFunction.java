package com.darkempire.degree.function;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.SettingClass;
import com.darkempire.math.struct.probability.Distribution;
import com.darkempire.math.struct.probability.ExponentialDistribution;
import com.darkempire.math.struct.probability.IDistribution;

import static com.darkempire.degree.SettingClass.claimsDistributionImpl;
import static com.darkempire.degree.SettingClass.incomeLambda;
import static com.darkempire.degree.SettingClass.thread_count;

/**
 * Created by siredvin on 29.04.15.
 *
 * @author siredvin
 */
public class ExponentialNumericDoubleFunction implements INumericFunction {
    private double coef_c;
    private double t_lambda;
    private double x_lambda;

    public ExponentialNumericDoubleFunction(double coef_c) {
        super();
        this.coef_c = coef_c;
        t_lambda = SettingClass.incomeLambda;
        x_lambda = claimsDistributionImpl.getAllParameters()[0];
    }

    public ExponentialNumericDoubleFunction(double coef_c, double t_lambda, double x_lambda) {
        this.coef_c = coef_c;
        this.t_lambda = t_lambda;
        this.x_lambda = x_lambda;
    }

    @Override
    public double calc(double x) {
        return (t_lambda * Math.exp(-(x_lambda - t_lambda / coef_c) * x)) / (x_lambda * coef_c);
    }

    @Override
    public void saveFunction() {
    }


    public static ExponentialNumericDoubleFunction approximate() {
        IDistribution claimsDistribution = SettingClass.claimsDistributionImpl;
        IDistribution incomeDistribution = SettingClass.incomeDistributionImpl;
        double mu_2 = claimsDistribution.getMoment(2), mu_3 = claimsDistribution.getMoment(3);
        double approx_x_lambda, approx_t_lambda, approx_c;
        if (claimsDistribution.getType() == Distribution.EXPONENTIAL) {
            approx_c = SettingClass.coef_c;
            approx_t_lambda = incomeLambda;
            approx_x_lambda = claimsDistribution.getAllParameters()[0];
        } else {
            approx_x_lambda = 3 * mu_2 / mu_3;
            approx_t_lambda = 9 * incomeDistribution.mean() * Math.pow(mu_2, 3) / (2 * Math.pow(mu_3, 2));
            approx_c = SettingClass.coef_c - incomeDistribution.mean() * claimsDistribution.mean() + approx_t_lambda / approx_x_lambda;
        }
        return new ExponentialNumericDoubleFunction(approx_c, approx_t_lambda, approx_x_lambda);
    }

    /**
     * Чорна магія якась, певно
     *
     * @param u початковий капітал
     * @return математичне очікування часу банкрутства, якщо він настає
     * @cite Steve Drekic and Gordon E. Willmot (2003). On the Density and Moments of the Time of Ruin with Exponential Claims. ASTIN Bulletin, 33, pp 11-21 doi:10.1017/S0515036100013271
     */
    public static double calculateMeanT_c(double u) {
        IDistribution claimsDistribution = SettingClass.claimsDistributionImpl;
        IDistribution incomeDistribution = SettingClass.incomeDistributionImpl;
        double mu_2 = claimsDistribution.getMoment(2), mu_3 = claimsDistribution.getMoment(3);
        Log.log(Log.debugIndex,1.1);
        double approx_x_lambda, approx_t_lambda, approx_c, approx_theta;
        if (claimsDistribution.getType() == Distribution.EXPONENTIAL) {
            approx_c = SettingClass.coef_c;
            approx_t_lambda = incomeLambda;
            approx_x_lambda = claimsDistribution.getAllParameters()[0];
            approx_theta = approx_c / (claimsDistribution.mean() * approx_t_lambda) - 1;
        } else {
            Log.log(Log.debugIndex,1.2);
            approx_x_lambda = 3 * mu_2 / mu_3;
            Log.log(Log.debugIndex,1.3);
            Log.log(Log.debugIndex,approx_x_lambda);
            IDistribution approx_claimsDistribution = new ExponentialDistribution(approx_x_lambda);
            Log.log(Log.debugIndex,1.4);
            approx_t_lambda = 9 * incomeDistribution.mean() * Math.pow(mu_2, 3) / (2 * Math.pow(mu_3, 2));
            Log.log(Log.debugIndex,1.5);
            approx_c = SettingClass.coef_c - incomeDistribution.mean() * claimsDistribution.mean() + approx_t_lambda / approx_x_lambda;
            Log.log(Log.debugIndex,1.6);
            approx_theta = approx_c / (approx_claimsDistribution.mean() * approx_t_lambda) - 1;
            Log.log(Log.debugIndex,1.7);
        }
        double R = approx_x_lambda * approx_theta / (1 + approx_theta);
        Log.log(Log.debugIndex,1.8);
        return (1 + approx_theta) * (R * u + approx_theta) / (approx_c * approx_theta * approx_theta * approx_x_lambda);
    }
}
