package com.darkempire.degree.function;

import com.darkempire.degree.module.ModelingModule;
import com.darkempire.degree.SettingClass;
import org.controlsfx.control.TaskProgressView;

/**
 * Created by siredvin on 29.04.15.
 *
 * @author siredvin
 */
public class CacheNumericDoubleFunction implements INumericFunction {
    private CacheNumericDoubleFunctionData data;
    private ModelingModule modelingModule;
    private TaskProgressView taskProgressView;

    protected CacheNumericDoubleFunction() {

    }

    public CacheNumericDoubleFunction(double[] params, ModelingModule modelingModule, TaskProgressView taskProgressView) {
        data = new CacheNumericDoubleFunctionData(CacheNumericDoubleFunctionController.searchBind(params));
        this.modelingModule = modelingModule;
        this.taskProgressView = taskProgressView;
    }

    @Override
    public double calc(double x) {
        if (data.contain(x)) {
            return data.get(x);
        }
        double result = modelingModule.blockRunModeling(SettingClass.length, SettingClass.count, SettingClass.thread_count, x, SettingClass.coef_c, taskProgressView, SettingClass.getRandom());
        data.put(x, result);
        return result;
    }

    @Override
    public void saveFunction() {
        data.saveFile();
    }
}
