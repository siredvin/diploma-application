package com.darkempire.degree.task;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.Main;
import com.darkempire.degree.MeanCutMethod;
import com.darkempire.math.struct.function.FGenerateDouble;
import javafx.concurrent.Task;

import java.util.Arrays;

/**
 * Created by siredvin on 14.05.15.
 *
 * @author siredvin
 */
public class MeanModelingTask extends Task<double[]> {
    /**
     * Допустимий відсоток траекторій, які не збанкрутували
     * При цьому відсотку дані будуть більш менш адекватні
     */
    public static final double GOOD_ESCAPE_PERCENT = 0.1;
    private FGenerateDouble xGenerator;
    private FGenerateDouble deltaGenerator;
    /**
     * Початковий капітал
     */
    private double u;
    /**
     * Страхові виплати
     */
    private double c;
    /**
     * Кількість кроків у одному моделюванні
     */
    private int length;
    /**
     * Кількість моделювань
     */
    private int count;
    /**
     * Унікальний номер моделюючого потоку
     */
    private byte number;
    /**
     * Кількість траекторій, які так і не збанкрутували
     */
    private int escapeNumber;

    /**
     * Метод обрізання моделювання (коли моделювання дійшло кінця, а банкрутства не сталося)
     */
    private MeanCutMethod cutMethod;

    private double[] times;

    public MeanModelingTask(FGenerateDouble xGenerator, FGenerateDouble deltaGenerator, double u, double c, int length, int count, byte number, MeanCutMethod cutMethod) {
        updateTitle("Моделювання середнього №" + number + ":Кількість кроків:" + count + ", довжина:" + length);
        this.xGenerator = xGenerator;
        this.deltaGenerator = deltaGenerator;
        this.c = c;
        this.u = u;
        this.cutMethod = cutMethod;
        this.times = new double[count];
        this.length = length;
        this.count = count;
        this.number = number;
    }

    /**
     * Загалом, відбувається count моделювань за схемою:
     * X_n =  X_{n-1} + c (t_n-t_{n-1} = delta) - xi_n
     * Де - (t_n - t_{n-1}) - відстань між страховими подіями, розподілена екпоненціально
     * xi_n - виплата за страхову подію. Розподіл задається користувачем
     *
     * @return кількість банкрутств, які відбулися під час моделювання
     * @throws Exception
     */
    @Override
    protected double[] call() throws Exception {
        Arrays.fill(times, -1);
        escapeNumber = 0;
        for (int path_counter = 0; path_counter < count; path_counter++) {
            double current_u = u;
            double escapedTime = 0;
            for (int time_counter = 0; time_counter < length; time_counter++) {
                double delta = deltaGenerator.next();
                escapedTime += delta;
                current_u += c * delta - xGenerator.next();
                if (current_u < 0) {
                    times[path_counter] = escapedTime;
                    if (escapedTime < 0) {
                        Log.error(Main.Log_index, new ArithmeticException("Час, що пройшов вийвився менше 0"));
                    }
                    break;
                }
            }
            if (times[path_counter] == -1) {
                escapeNumber++;
                switch (cutMethod) {
                    case IGNORE:
                        times[path_counter] = -1;
                        break;
                    case ADD_MAX:
                        times[path_counter] = escapedTime;
                        break;
                }
            }
            updateProgress(path_counter, count);
        }
        updateProgress(1, 1);
        return times;
    }


    public double getC() {
        return c;
    }

    public int getLength() {
        return length;
    }

    public int getCount() {
        return count;
    }

    public int getEscapeNumber() {
        return escapeNumber;
    }

    public double getEscapePercent() {
        return ((double) escapeNumber) / count;
    }

    public byte getNumber() {
        return number;
    }

    public double[] getTimes() {
        return times;
    }

    @Override
    public String toString() {
        return "\nРазові виплати:" + c + "\nКількість кроків для моделювання:" + length + "\nКількість траекторій для моделювання:" + count + "\nНомер:" + number;
    }
}
