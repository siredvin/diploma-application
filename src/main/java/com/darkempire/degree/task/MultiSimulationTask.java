package com.darkempire.degree.task;

import com.darkempire.anji.util.DoubleParameters;
import com.darkempire.math.struct.function.FGenerateDouble;
import javafx.concurrent.Task;

/**
 * Created by siredvin on 23.09.14.
 */
public class MultiSimulationTask extends Task<double[]> {
    private FGenerateDouble xGenerator;
    private FGenerateDouble deltaGenerator;
    /**
     * Страхові виплати
     */
    private double c;
    /**
     * Кількість кроків у одному моделюванні
     */
    private int length;
    /**
     * Кількість моделювань
     */
    private int count;
    /**
     * Унікальний номер моделюючого потоку
     */
    private byte number;

    private double[] points;

    public MultiSimulationTask(FGenerateDouble xGenerator, FGenerateDouble deltaGenerator, double c, int length, int count, byte number) {
        updateTitle("Пресимуляція №" + number + ":Кількість кроків:" + count + ", довжина:" + length);
        this.xGenerator = xGenerator;
        this.deltaGenerator = deltaGenerator;
        this.c = c;
        this.points = new double[count];
        this.length = length;
        this.count = count;
        this.number = number;
    }

    /**
     * Загалом, відбувається count моделювань за схемою:
     * X_n =  X_{n-1} + c (t_n-t_{n-1} = delta) - xi_n
     * Де - (t_n - t_{n-1}) - відстань між страховими подіями, розподілена екпоненціально
     * xi_n - виплата за страхову подію. Розподіл задається користувачем
     *
     * @return кількість банкрутств, які відбулися під час моделювання
     * @throws Exception
     */
    @Override
    protected double[] call() throws Exception {
        for (int path_counter = 0; path_counter < count; path_counter++) {
            double current_u = 0;
            double min_u = current_u;
            for (int time_counter = 0; time_counter < length; time_counter++) {
                current_u += c * deltaGenerator.next() - xGenerator.next();
                if (current_u < min_u) {
                    min_u = current_u;
                }
            }
            points[path_counter] = min_u;
            updateProgress(path_counter, count);
        }
        updateProgress(1, 1);
        return points;
    }


    public double getC() {
        return c;
    }

    public int getLength() {
        return length;
    }

    public int getCount() {
        return count;
    }

    public byte getNumber() {
        return number;
    }

    public double[] getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return "\nРазові виплати:" + c + "\nКількість кроків для моделювання:" + length + "\nКількість траекторій для моделювання:" + count;
    }
}
