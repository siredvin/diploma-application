package com.darkempire.degree.task;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.function.CacheNumericDoubleFunction;
import com.darkempire.degree.module.ModelingModule;
import com.darkempire.degree.SettingClass;
import com.darkempire.math.MathMachine;
import com.darkempire.math.utils.ArrayUtils;
import javafx.concurrent.Task;
import org.controlsfx.control.TaskProgressView;

/**
 * Created by siredvin on 09.05.15.
 *
 * @author siredvin
 */
public class FunctionCalculateTask extends Task<Void> {
    private double startU, endU;
    private int saveCount;
    private ModelingModule modelingModule;
    private TaskProgressView taskProgressView;

    public FunctionCalculateTask(double startU, double endU, int saveCount, ModelingModule modelingModule, TaskProgressView taskProgressView) {
        this.startU = startU;
        this.endU = endU;
        this.saveCount = saveCount;
        this.modelingModule = modelingModule;
        this.taskProgressView = taskProgressView;
        updateTitle("Попереднє обчислення функції від " + MathMachine.format(startU) + " до " + MathMachine.format(endU));
    }

    @Override
    protected Void call() throws Exception {
        double[] params = new double[]{SettingClass.coef_c, SettingClass.incomeLambda, SettingClass.claimsDistribution.ordinal()};
        params = ArrayUtils.append(params, SettingClass.claimsParams);
        CacheNumericDoubleFunction f = new CacheNumericDoubleFunction(params, modelingModule, taskProgressView);
        int counter = 0;
        double length = endU - startU;
        updateProgress(0, 1);
        for (double u = startU; u <= endU; u += MathMachine.DERIVATE_DELTA) {
            f.calc(u);
            updateProgress(u - startU, length);
            counter++;
            if (counter == saveCount) {
                counter = 0;
                f.saveFunction();
            }
        }
        f.saveFunction();
        updateProgress(1, 1);
        return null;
    }
}
