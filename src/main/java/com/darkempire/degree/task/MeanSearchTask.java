package com.darkempire.degree.task;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.degree.SettingClass;
import com.darkempire.degree.function.INumericFunction;
import com.darkempire.degree.function.IntervalNumericDoubleFunction;
import com.darkempire.degree.module.ModelingModule;
import com.darkempire.math.utils.ArrayUtils;
import javafx.concurrent.Task;
import org.controlsfx.control.TaskProgressView;

import static com.darkempire.math.operator.special.Integral.finiteIntegrate;

/**
 * Created by siredvin on 29.04.15.
 *
 * @author siredvin
 */
public class MeanSearchTask extends Task<Double> {
    private TaskProgressView taskProgressView;
    private ModelingModule modelingModule;
    private INumericFunction f;
    private double start_cash;

    public MeanSearchTask(ModelingModule modelingModule, TaskProgressView taskProgressView) {
        this.modelingModule = modelingModule;
        this.taskProgressView = taskProgressView;
        this.start_cash = SettingClass.start_cash;
        this.f = null;
        updateTitle("Обчислення математичного середнього");
    }

    public MeanSearchTask(double start_cash, ModelingModule modelingModule, TaskProgressView taskProgressView) {
        this.modelingModule = modelingModule;
        this.taskProgressView = taskProgressView;
        this.start_cash = start_cash;
        this.f = null;
        updateTitle("Обчислення математичного середнього");
    }

    public void setStartCash(double cash) {
        start_cash = cash;
    }

    public void setFunction(INumericFunction function) {
        this.f = function;
    }

    @Override
    protected Double call() throws Exception {
        double[] params = new double[]{SettingClass.coef_c, SettingClass.incomeLambda, SettingClass.claimsDistribution.ordinal()};
        params = ArrayUtils.append(params, SettingClass.claimsParams);
        if (f == null) {
            f = IntervalNumericDoubleFunction.loadFunction(params, modelingModule, taskProgressView);
            if (f == null) {
                MonologGeneratorPane.showErrorDialog("Дуже страшна помилка !!!!");
                return null;
            }
        }
        double u = start_cash;
        //Знаходження фукнції psi
        double psi = f.calc(u);
        //Знаходження моментів розподілу
        double p1 = SettingClass.claimsDistributionImpl.getMoment(1);
        double p2 = SettingClass.claimsDistributionImpl.getMoment(2);
        //Знаходження theta, яка premium loading factor
        double theta = SettingClass.coef_c / (SettingClass.incomeLambda * p1) - 1;
        //Обчислення фукнції psi_1
        updateProgress(0.1, 1);
        double temp1 = finiteIntegrate(0, u, x -> f.calc(x) * (1 - f.calc(u - x)));
        updateProgress(0.8, 1);
        double psi_1 = (p2 * (1 - f.calc(u)) / (2 * theta * p1) - temp1) / (SettingClass.incomeLambda * p1 * theta);
        f.saveFunction();
        Log.log(Log.debugIndex, "temp1=", temp1, '\n', "psi1=", psi_1, '\n', "theta=", theta, '\n', "psi=", psi, '\n', "result=", psi_1 / psi);
        updateProgress(1, 1);
        return psi_1 / psi;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
