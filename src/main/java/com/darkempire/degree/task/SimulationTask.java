package com.darkempire.degree.task;

import com.darkempire.math.struct.function.FGenerateDouble;
import javafx.concurrent.Task;

/**
 * Created by siredvin on 23.09.14.
 */
public class SimulationTask extends Task<Integer> {
    private FGenerateDouble xGenerator;
    private FGenerateDouble deltaGenerator;
    /**
     * Початковий капітал
     */
    private double u;
    /**
     * Страхові виплати
     */
    private double c;
    /**
     * Кількість кроків у одному моделюванні
     */
    private int length;
    /**
     * Кількість моделювань
     */
    private int count;
    /**
     * Унікальний номер моделюючого потоку
     */
    private byte number;

    private int fail_count;

    public SimulationTask(FGenerateDouble xGenerator, FGenerateDouble deltaGenerator, double u, double c, int length, int count, byte number) {
        updateTitle("Симуляція №" + number + ":Кількість кроків:" + count + ", довжина:" + length);
        this.xGenerator = xGenerator;
        this.deltaGenerator = deltaGenerator;
        this.u = u;
        this.c = c;
        this.length = length;
        this.count = count;
        this.number = number;
    }

    /**
     * Загалом, відбувається count моделювань за схемою:
     * X_n =  X_{n-1} + c (t_n-t_{n-1} = delta) - xi_n
     * Де - (t_n - t_{n-1}) - відстань між страховими подіями, розподілена екпоненціально
     * xi_n - виплата за страхову подію. Розподіл задається користувачем
     *
     * @return кількість банкрутств, які відбулися під час моделювання
     * @throws Exception
     */
    @Override
    protected Integer call() throws Exception {
        int fail_count = 0;
        for (int path_counter = 0; path_counter < count; path_counter++) {
            double current_u = u;
            for (int time_counter = 0; time_counter < length; time_counter++) {
                current_u += c * deltaGenerator.next() - xGenerator.next();
                if (current_u < 0) {
                    fail_count++;
                    break;
                }
            }
            updateProgress(path_counter, count);
        }
        updateProgress(1, 1);
        this.fail_count = fail_count;
        return fail_count;
    }


    public double getU() {
        return u;
    }

    public double getC() {
        return c;
    }

    public int getLength() {
        return length;
    }

    public int getCount() {
        return count;
    }

    public byte getNumber() {
        return number;
    }

    public int getFail_count() {
        return fail_count;
    }

    @Override
    public String toString() {
        return "\nПочатковий капітал:" + u + "\nРазові виплати:" + c + "\nКількість кроків для моделювання:" + length + "\nКількість траекторій для моделювання:" + count;
    }
}
