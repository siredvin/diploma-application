package com.darkempire.degree.task;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.scene.chart.AnjiLineChart;
import com.darkempire.degree.SettingClass;
import com.darkempire.degree.function.ExponentialNumericDoubleFunction;
import com.darkempire.degree.function.INumericFunction;
import com.darkempire.degree.module.ModelingModule;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.scene.chart.XYChart;
import org.controlsfx.control.TaskProgressView;

import java.util.Set;
import java.util.StringJoiner;

/**
 * Created by siredvin on 20.05.15.
 *
 * @author siredvin
 */
public class AddChartTask extends Task<Void> {
    private ModelingModule modelingModule;
    private TaskProgressView taskProgressView;
    private AnjiLineChart<Number, Number> compareChart;
    private double startCash, endCash;
    private int stepCount;
    private boolean addFunction, addApproximation, addModeling;

    public AddChartTask(double startCash, double endCash, int stepCount, AnjiLineChart<Number, Number> compareChart,
                        ModelingModule modelingModule, TaskProgressView taskProgressView, boolean addFunction, boolean addApproximation, boolean addModeling) {
        this.startCash = startCash;
        this.endCash = endCash;
        this.stepCount = stepCount;
        this.modelingModule = modelingModule;
        this.taskProgressView = taskProgressView;
        this.compareChart = compareChart;
        this.addApproximation = addApproximation;
        this.addModeling = addModeling;
        this.addFunction = addFunction;
        StringJoiner joiner = new StringJoiner(",", "Додавання до графіку:", "");
        if (addApproximation) {
            joiner.add("апроксимації");
        }
        if (addModeling) {
            joiner.add("моделювання");
        }
        if (addFunction) {
            joiner.add("фукнції");
        }
        updateTitle(joiner.toString());
    }

    @Override
    protected Void call() throws Exception {
        double stepDelta = (endCash - startCash) / stepCount;
        XYChart.Series<Number, Number> functionSeries = new XYChart.Series<>("T_c (функція)", FXCollections.observableArrayList());
        XYChart.Series<Number, Number> approximationSeries = new XYChart.Series<>("T_c (апроксимація)", FXCollections.observableArrayList());
        XYChart.Series<Number, Number> modelingSeries = new XYChart.Series<>("Т_с (моделювання)", FXCollections.observableArrayList());
        updateProgress(0, 1);
        for (double cash = startCash; cash <= endCash; cash += stepDelta) {
            Log.log(Log.debugIndex,cash);
            if (addFunction) {
                MeanSearchTask functionTask = new MeanSearchTask(cash, modelingModule, taskProgressView);
                Platform.runLater(() -> taskProgressView.getTasks().add(functionTask));
                double functionValue = functionTask.call();
                Platform.runLater(() -> taskProgressView.getTasks().remove(functionTask));
                functionSeries.getData().add(new XYChart.Data<>(cash, functionValue));
            }

            if (addApproximation) {
                Log.log(Log.debugIndex,1);
                double approximateValue = ExponentialNumericDoubleFunction.calculateMeanT_c(cash);
                Log.log(Log.debugIndex,2);
                approximationSeries.getData().add(new XYChart.Data<>(cash, approximateValue));
            }

            if (addModeling) {
                double modelingValue = modelingModule.blockMeanModeling(SettingClass.length, SettingClass.count, SettingClass.thread_count, cash, SettingClass.coef_c, taskProgressView, SettingClass.getRandom());
                modelingSeries.getData().add(new XYChart.Data<>(cash, modelingValue));
            }

            updateProgress(cash - startCash, endCash - startCash);
        }
        updateProgress(1, 1);
        Platform.runLater(() -> {
            if (addApproximation) {
                Log.log(Log.debugIndex,"Додана апроксимація");
                compareChart.getData().add(approximationSeries);
            }
            if (addFunction) {
                Log.log(Log.debugIndex,"Додана функція");
                compareChart.getData().add(functionSeries);
            }
            if (addModeling) {
                Log.log(Log.debugIndex,"Додане моделюванн");
                compareChart.getData().add(modelingSeries);
            }
        });
        return null;
    }
}
