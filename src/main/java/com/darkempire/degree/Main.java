package com.darkempire.degree;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.controller.MainController;
import com.darkempire.math.MathMachine;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Created by siredvin on 23.09.14.
 *
 * @author siredvin
 */
public class Main extends Application {
    public static final int Log_index = Log.addChanel("Degree Log");

    public static void main(String[] args) {
        Main.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        MathMachine.numberFormat.setMaximumFractionDigits(10);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("/main.fxml"));
        Image image = new Image(Main.class.getResourceAsStream("/icon.png"));
        Scene scene = new Scene(loader.load());
        MainController controller = loader.getController();
        controller.init(primaryStage);
        primaryStage.getIcons().add(image);
        primaryStage.setTitle("Моделювання траекторій для диплому");
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }
}