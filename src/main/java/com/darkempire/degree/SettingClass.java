package com.darkempire.degree;

import com.darkempire.anji.annotation.AnjiUtil;
import com.darkempire.anji.log.Log;
import com.darkempire.degree.module.RandomModule;
import com.darkempire.degree.function.CacheNumericDoubleFunctionController;
import com.darkempire.math.struct.probability.Distribution;
import com.darkempire.math.struct.probability.ExponentialDistribution;
import com.darkempire.math.struct.probability.IDistribution;
import com.darkempire.math.utils.ArrayUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

/**
 * Created by siredvin on 04.02.15.
 *
 * @author siredvin
 */
@AnjiUtil
public final class SettingClass {

    private SettingClass() {
    }

    public static int length;
    public static int count;
    public static double start_cash;
    public static double coef_c;
    public static int thread_count;
    public static double[] claimsParams;
    public static double incomeLambda;
    public static Distribution claimsDistribution;
    public static double randomEpsBarrier;
    public static boolean isSystemRandom;
    public static MeanCutMethod cutMethod;

    public static IDistribution claimsDistributionImpl;
    public static IDistribution incomeDistributionImpl;
    public static RandomModule randomModule;

    public final static File settingFile = new File("setting.xml");

    static {
        claimsDistribution = Distribution.EXPONENTIAL;
        claimsParams = new double[]{0.3};
        incomeLambda = 0.3;
        start_cash = 10000;
        coef_c = 5;
        thread_count = 3;
        length = 5000;
        count = 5000;
        randomEpsBarrier = 1e-4;
        isSystemRandom = true;
        randomModule = new RandomModule();
        cutMethod = MeanCutMethod.ADD_MAX;
    }

    public static RandomModule getRandomModule() {
        return randomModule;
    }

    public static Random getRandom() {
        return randomModule.getRandom();
    }

    public static void loadSetting() {
        Properties properties = new Properties();
        try {
            properties.loadFromXML(new FileInputStream(settingFile));
            length = Integer.parseInt(properties.getProperty("length", String.valueOf(length)));
            count = Integer.parseInt(properties.getProperty("count", String.valueOf(count)));
            start_cash = Double.parseDouble(properties.getProperty("start_cash", String.valueOf(start_cash)));
            coef_c = Double.parseDouble(properties.getProperty("coef_c", String.valueOf(coef_c)));
            thread_count = Integer.parseInt(properties.getProperty("thread_count", String.valueOf(thread_count)));
            claimsDistribution = Distribution.valueOf(properties.getProperty("claimsDistribution", claimsDistribution.name()));
            incomeLambda = Double.parseDouble(properties.getProperty("incomeLambda", String.valueOf(incomeLambda)));
            randomEpsBarrier = Double.parseDouble(properties.getProperty("randomEpsBarrier", String.valueOf(randomEpsBarrier)));
            isSystemRandom = Boolean.parseBoolean(properties.getProperty("isSystemRandom", String.valueOf(isSystemRandom)));
            cutMethod = MeanCutMethod.valueOf(properties.getProperty("cutMethod", cutMethod.name()));
            String paramsString = properties.getProperty("claimsParams");
            if (paramsString != null) {
                claimsParams = ArrayUtils.valueOf(paramsString);
            }
            recreate();
        } catch (IOException e) {
            Log.error(Log.logIndex, e);
        }
    }

    public static void saveSetting() {
        refind();
        CacheNumericDoubleFunctionController.dump();
        Properties properties = new Properties();
        properties.setProperty("length", String.valueOf(length));
        properties.setProperty("count", String.valueOf(count));
        properties.setProperty("thread_count", String.valueOf(thread_count));
        properties.setProperty("start_cash", String.valueOf(start_cash));
        properties.setProperty("coef_c", String.valueOf(coef_c));
        properties.setProperty("claimsDistribution", claimsDistribution.name());
        properties.setProperty("claimsParams", ArrayUtils.dumpArray(claimsParams));
        properties.setProperty("incomeLambda", String.valueOf(incomeLambda));
        properties.setProperty("randomEpsBarrier", String.valueOf(randomEpsBarrier));
        properties.setProperty("isSystemRandom", String.valueOf(isSystemRandom));
        properties.setProperty("cutMethod", cutMethod.name());
        try {
            properties.storeToXML(new FileOutputStream(settingFile), "Some setting file");
        } catch (IOException e) {
            Log.error(Log.logIndex, e);
        }
    }


    public static void recreate() {
        incomeDistributionImpl = new ExponentialDistribution(incomeLambda);
        claimsDistributionImpl = claimsDistribution.getDistribution(claimsParams);
        if (isSystemRandom) {
            randomModule.clear();
        } else {
            randomModule.searchRandomGenerators(randomEpsBarrier);
        }
    }

    public static void refind() {
        incomeLambda = incomeDistributionImpl.getAllParameters()[0];
        claimsParams = claimsDistributionImpl.getAllParameters();
        claimsDistribution = claimsDistributionImpl.getType();
        isSystemRandom = randomModule.getSize() == 0;
        randomEpsBarrier = randomModule.getEps();
    }
}
