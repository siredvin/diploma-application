package com.darkempire.degree;

import com.darkempire.anji.log.Log;
import com.darkempire.degree.function.ExponentialNumericDoubleFunction;
import com.darkempire.degree.function.INumericFunction;
import com.darkempire.degree.function.IntervalNumericDoubleFunction;
import com.darkempire.degree.module.ModelingModule;
import com.darkempire.math.utils.ArrayUtils;

import java.io.*;
import java.util.Arrays;

/**
 * Created by siredvin on 28.04.15.
 *
 * @author siredvin
 */
public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SettingClass.loadSetting();
        /*double[] params = new double[]{SettingClass.coef_c,SettingClass.incomeLambda,SettingClass.claimsDistribution.ordinal()};
        params = ArrayUtils.append(params, SettingClass.claimsParams);
        Log.log(Log.debugIndex,params);
        ModelingModule controller = new ModelingModule();
        INumericFunction trueFunction = new TestNumericDoubleFunction(SettingClass.coef_c);
        INumericFunction f = IntervalNumericDoubleFunction.loadFunction(params,controller,null);
        Log.log(Log.debugIndex, 1-f.calc(8));
        Log.log(Log.debugIndex,1-trueFunction.calc(8));*/
        double[] params = new double[]{SettingClass.coef_c, SettingClass.incomeLambda, SettingClass.claimsDistribution.ordinal()};
        params = ArrayUtils.append(params, SettingClass.claimsParams);
        ModelingModule controller = new ModelingModule();
        IntervalNumericDoubleFunction function = IntervalNumericDoubleFunction.loadFunction(params, controller, null);
        /*for (double d:points){
            Log.log(Log.debugIndex,function.calc(d));
        }*/
        Log.log(Log.debugIndex, function.getData());
    }
}
