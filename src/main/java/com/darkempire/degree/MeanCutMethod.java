package com.darkempire.degree;

/**
 * Created by siredvin on 14.05.15.
 *
 * @author siredvin
 */
public enum MeanCutMethod {
    /**
     * Для ігнорування використовується спеціальне позначення (-1), яке ігнорується
     */
    IGNORE,
    ADD_MAX;


    @Override
    public String toString() {
        String name = null;
        switch (this) {
            case IGNORE:
                name = "Ігнорування";
                break;
            case ADD_MAX:
                name = "Додавання максимального";
                break;
        }
        return name;
    }
}
