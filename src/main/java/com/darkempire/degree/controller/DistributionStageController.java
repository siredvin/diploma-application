package com.darkempire.degree.controller;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.monolog.MonologController;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.anjifx.scene.DoubleField;
import com.darkempire.math.MathMachine;
import com.darkempire.math.struct.probability.Distribution;
import com.darkempire.math.struct.probability.IDistribution;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.DeflaterInputStream;

/**
 * Created by siredvin on 23.02.15.
 *
 * @author siredvin
 */
public class DistributionStageController {
    @FXML
    private VBox parametersBox;

    @FXML
    private ComboBox<Distribution> distributionBox;
    private Stage stage;
    private IDistribution result;
    private boolean isEdited;
    private List<Spinner<Double>> doubleFieldList;

    @FXML
    void acceptAction(ActionEvent event) {
        Distribution currentDistribution = distributionBox.getValue();
        int count = currentDistribution.getParametersCount();
        double[] params = new double[count];
        for (int i = 0; i < count; i++) {
            params[i] = doubleFieldList.get(i).getValue();
        }
        result = currentDistribution.getDistribution(params);
        if (Math.abs(result.mean() - 1) < MathMachine.MACHINE_PRACTICAL_EPS) {
            isEdited = true;
            stage.close();
        } else {
            MonologGeneratorPane.showErrorDialog("Такі параметри не підходять", "Математичне очікування має дорівнювати одиниці");
        }
    }

    @FXML
    void cancelAction(ActionEvent event) {
        isEdited = false;
        doubleFieldList.clear();
        stage.close();
    }

    @FXML
    void initialize() {
        assert parametersBox != null : "fx:id=\"parametersBox\" was not injected: check your FXML file 'distribution.fxml'.";
        assert distributionBox != null : "fx:id=\"distributionBox\" was not injected: check your FXML file 'distribution.fxml'.";
        distributionBox.getItems().addAll(Distribution.values());
        distributionBox.getItems().remove(Distribution.SPECIAL);
        isEdited = false;
        distributionBox.getSelectionModel().selectedItemProperty().addListener(this::changeListener);
        doubleFieldList = new ArrayList<>();
    }

    public void init(Stage stage, IDistribution distribution) {
        this.result = distribution;
        this.stage = stage;
        distributionBox.getSelectionModel().select(distribution.getType());
    }

    public IDistribution getResult() {
        return result;
    }

    public boolean isEdited() {
        return isEdited;
    }


    private void changeListener(ObservableValue<? extends Distribution> observable, Distribution oldValue, Distribution newValue) {
        int count = newValue.getParametersCount();
        double[] params = result.getAllParameters();
        String[] parametersNames = newValue.getParametersNames();
        parametersBox.getChildren().clear();
        doubleFieldList.clear();
        for (int i = 0; i < count; i++) {
            HBox hBox = new HBox();
            hBox.setSpacing(5);
            hBox.setPadding(new Insets(5));
            Label label = new Label(parametersNames[i]);
            Spinner<Double> spinner = new Spinner<>();
            spinner.setEditable(true);
            if (params.length == parametersNames.length) {
                spinner.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, Double.MAX_VALUE, params[i], MathMachine.MACHINE_HALF_EPS));
            } else {
                spinner.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, Double.MAX_VALUE, 0, MathMachine.MACHINE_HALF_EPS));
            }
            doubleFieldList.add(spinner);
            hBox.getChildren().addAll(label, spinner);
            parametersBox.getChildren().add(hBox);
        }
        stage.sizeToScene();
    }
}
