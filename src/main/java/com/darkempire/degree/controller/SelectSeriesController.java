package com.darkempire.degree.controller;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.monolog.MonologController;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.anjifx.scene.chart.AnjiLineChart;
import com.darkempire.anjifx.util.AnjiFXMathConverter;
import com.darkempire.math.struct.matrix.DoubleArrayMatrix;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import org.controlsfx.control.ListSelectionView;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by siredvin on 20.05.15.
 *
 * @author siredvin
 */
public class SelectSeriesController {

    @FXML
    private ListSelectionView<XYChart.Series<Number, Number>> listSelectionView;

    private Stage stage;
    private AnjiLineChart<Number, Number> compareChart;
    private boolean isAccept;

    @FXML
    void acceptAction(ActionEvent event) {
        isAccept = true;
        stage.close();
    }

    public ObservableList<XYChart.Series<Number, Number>> getTarget() {
        return listSelectionView.getTargetItems();
    }


    public boolean isAccept() {
        return isAccept;
    }


    @FXML
    void cancelAction(ActionEvent event) {
        isAccept = false;
        stage.close();
    }

    public void init(Stage stage, AnjiLineChart<Number, Number> compareChart) {
        this.stage = stage;
        this.compareChart = compareChart;
        listSelectionView.getSourceItems().addAll(compareChart.getData());
        ((Label) listSelectionView.getSourceHeader()).setText("Доступні");
        ((Label) listSelectionView.getTargetHeader()).setText("Обрані");
    }

    @FXML
    void initialize() {
        listSelectionView.setCellFactory(param -> new SeriesListCell());
    }

    private class SeriesListCell extends ListCell<XYChart.Series<Number, Number>> {

        @Override
        protected void updateItem(XYChart.Series<Number, Number> item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                int index = compareChart.getData().indexOf(item);
                setText(item.getName());
                if (index > -1) {
                    setGraphic(new Rectangle(10, 10, compareChart.lineStrokeColor(index).getValue()));
                }
            }
        }
    }
}
