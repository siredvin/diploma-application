package com.darkempire.degree.controller;

import com.darkempire.anji.internal.anji.AnjiCoreLocalHolder;
import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.beans.property.AnjiDoubleProperty;
import com.darkempire.anjifx.beans.property.AnjiIntegerProperty;
import com.darkempire.anjifx.dialog.DialogUtil;
import com.darkempire.anjifx.dialog.property.PropertyEditDialog;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.anjifx.scene.chart.AnjiLineChart;
import com.darkempire.anjifx.util.AnjiFXMathConverter;
import com.darkempire.degree.Main;
import com.darkempire.degree.SettingClass;
import com.darkempire.degree.function.INumericFunction;
import com.darkempire.degree.task.AddChartTask;
import com.darkempire.degree.task.FunctionCalculateTask;
import com.darkempire.degree.module.AnalyzeModule;
import com.darkempire.degree.module.ModelingModule;
import com.darkempire.degree.module.SampleSearchModule;
import com.darkempire.degree.task.MeanSearchTask;
import com.darkempire.degree.function.ExponentialNumericDoubleFunction;
import com.darkempire.math.struct.matrix.DoubleArrayMatrix;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import com.darkempire.math.struct.probability.Distribution;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.TaskProgressView;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * Created by siredvin on 23.09.14.
 *
 * @author siredvin
 */
public class MainController {
    @FXML
    private TaskProgressView taskProgressView;
    @FXML
    private AnjiLineChart<Number, Number> compareChart;
    @FXML
    private AnjiLineChart analyzeChart;

    private ModelingModule modelingModule;
    private boolean freeAnalyze;

    private Stage stage;

    @FXML
    void initialize() {
        SettingClass.loadSetting();
        ((NumberAxis) compareChart.getXAxis()).setForceZeroInRange(false);
        ((NumberAxis) compareChart.getYAxis()).setForceZeroInRange(false);
        compareChart.setAnimated(false);
        modelingModule = new ModelingModule();
        analyzeChart.setVerticalZeroLineVisible(false);
        analyzeChart.setHorizontalZeroLineVisible(false);
        ((NumberAxis) analyzeChart.getXAxis()).setForceZeroInRange(false);
        ((NumberAxis) analyzeChart.getYAxis()).setForceZeroInRange(false);
        freeAnalyze = true;
    }

    public void init(Stage ownerStage) {
        this.stage = ownerStage;
        stage.setOnCloseRequest(event -> SettingClass.saveSetting());
    }

    @FXML
    private void analyzePathLength(ActionEvent event) {
        if (freeAnalyze) {
            AnjiIntegerProperty countProperty = new AnjiIntegerProperty("Кількість шляхів", SettingClass.count),
                    modelingCountProperty = new AnjiIntegerProperty("Кількість запусків", 1),
                    startLengthProperty = new AnjiIntegerProperty("Початкова довжина шляху", SettingClass.length / 2),
                    endLengthProperty = new AnjiIntegerProperty("Кінцева довжина шляху", 2 * SettingClass.length),
                    stepProperty = new AnjiIntegerProperty("Крок", 500);
            PropertyEditDialog ped = DialogUtil.createDialog("Оберіть параметри", countProperty, startLengthProperty, endLengthProperty, stepProperty, modelingCountProperty);
            ped.showAndWait();

            if (ped.isEdited()) {
                freeAnalyze = false;
                double startCash = SettingClass.start_cash;
                double actuarPaiment = SettingClass.coef_c;
                int modelingCount = modelingCountProperty.get();
                List<XYChart.Series<Double, Double>> series = new ArrayList<>(modelingCount);
                for (int i = 0; i < modelingCount; i++) {
                    series.add(new XYChart.Series<>("Зміна довжини (кількість:" + countProperty.get() + ")", FXCollections.observableArrayList()));
                }
                analyzeChart.getData().addAll(series);
                modelingModule.setShowMessage(false);
                AnalyzeModule analyzer = new AnalyzeModule(true, countProperty.get(),
                        startLengthProperty.get(), endLengthProperty.get(), stepProperty.get(), SettingClass.thread_count, startCash, actuarPaiment,
                        series, modelingModule, taskProgressView, modelingCount);
                analyzer.setFinishAction(() -> {
                    modelingModule.setShowMessage(true);
                    MonologGeneratorPane.showAcceptDialog("Графік залежності намальований");
                    freeAnalyze = true;
                });
                analyzer.start();
            }
        }
    }

    @FXML
    private void analyzePathCount(ActionEvent event) {
        if (freeAnalyze) {
            AnjiIntegerProperty lengthProperty = new AnjiIntegerProperty("Довжина шляху", SettingClass.length),
                    modelingCountProperty = new AnjiIntegerProperty("Кількість запусків", 1),
                    startCountProperty = new AnjiIntegerProperty("Початкова кількість шляхів", SettingClass.count / 2),
                    endCountProperty = new AnjiIntegerProperty("Кінцева кількість шляхів", 2 * SettingClass.count),
                    stepProperty = new AnjiIntegerProperty("Крок", 500);
            PropertyEditDialog ped = DialogUtil.createDialog("Оберіть параметри", lengthProperty, startCountProperty, endCountProperty, stepProperty, modelingCountProperty);
            ped.showAndWait();

            if (ped.isEdited()) {
                freeAnalyze = false;
                double startCash = SettingClass.start_cash;
                double actuarPaiment = SettingClass.coef_c;
                int modelingCount = modelingCountProperty.get();
                List<XYChart.Series<Double, Double>> series = new ArrayList<>(modelingCount);
                for (int i = 0; i < modelingCount; i++) {
                    series.add(new XYChart.Series<>("Зміна кількості (довжина" + lengthProperty.get() + ")", FXCollections.observableArrayList()));
                }
                analyzeChart.getData().addAll(series);
                modelingModule.setShowMessage(false);
                AnalyzeModule analyzer = new AnalyzeModule(false, lengthProperty.get(),
                        startCountProperty.get(), endCountProperty.get(), stepProperty.get(), SettingClass.thread_count, startCash, actuarPaiment,
                        series, modelingModule, taskProgressView, modelingCount);
                analyzer.setFinishAction(() -> {
                    modelingModule.setShowMessage(true);
                    MonologGeneratorPane.showAcceptDialog("Графік залежності намальований");
                    freeAnalyze = true;
                });
                analyzer.start();
            }
        }
    }

    @FXML
    private void saveImage(ActionEvent event) {
        WritableImage image = analyzeChart.snapshot(new SnapshotParameters(), null);
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(AnjiCoreLocalHolder.getString("anjifx.scene.charts.tabchart.pngimage"), "*.png"));
        File f = chooser.showSaveDialog(stage);
        if (f != null) {
            if (!f.getAbsolutePath().contains(".png")) {
                f = new File(f.getAbsolutePath() + ".png");
            }
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", f);
            } catch (IOException e) {
                Log.error(Main.Log_index, e);
                MonologGeneratorPane.showErrorDialog("Помилка збереження", e.getMessage());
            }
        }
    }

    @FXML
    private void clearChart(ActionEvent event) {
        if (MonologGeneratorPane.showQuestionDialog("Ви певні?", "Точно очистити графік?")) {
            analyzeChart.getData().clear();
        }
    }

    @FXML
    private void saveData(ActionEvent event) {
        if (analyzeChart.getData().isEmpty()) {
            MonologGeneratorPane.showAcceptDialog("Зберігати нічого");
            return;
        }
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Таблиця даних графіку", "*.cdt"));
        File f = chooser.showSaveDialog(stage);
        if (f != null) {
            if (!f.getAbsolutePath().contains(".cdt")) {
                f = new File(f.getAbsolutePath() + ".cdt");
            }
            try {
                PrintWriter pw = new PrintWriter(new FileOutputStream(f));
                for (Object series : analyzeChart.getData()) {
                    XYChart.Series s = (XYChart.Series) series;
                    pw.print(s.getName());
                    pw.print('\t');
                    for (Object data : s.getData()) {
                        XYChart.Data d = (XYChart.Data) data;
                        pw.print(d.getXValue());
                        pw.print(';');
                        pw.print(d.getYValue());
                        pw.print('\t');
                    }
                    pw.println();
                }
                pw.close();
            } catch (IOException e) {
                Log.error(Main.Log_index, e);
                MonologGeneratorPane.showErrorDialog("Помилка збереження", e.getMessage());
            }
        }
    }

    @FXML
    private void sampleSearchAction(ActionEvent event) {
        if (freeAnalyze) {
            if (SettingClass.claimsDistributionImpl.getType() != Distribution.EXPONENTIAL) {
                MonologGeneratorPane.showErrorDialog("Неправильний розподіл!", "Для пошуку вибірки розподіл має бути експоненціальним");
                return;
            }
            analyzeChart.getData().clear();
            AnjiIntegerProperty lengthProperty = new AnjiIntegerProperty("Довжина шляху", SettingClass.length),
                    countProperty = new AnjiIntegerProperty("Кількість шляхів", SettingClass.count),
                    iterateCountProperty = new AnjiIntegerProperty("Кількість ітерацій", 10);
            AnjiDoubleProperty epsProperty = new AnjiDoubleProperty("Необхідна точність", 1E-4);
            PropertyEditDialog ped = DialogUtil.createDialog("Оберіть параметри", lengthProperty, countProperty, epsProperty, iterateCountProperty);
            ped.showAndWait();
            if (ped.isEdited()) {
                freeAnalyze = false;
                modelingModule.setShowMessage(false);
                XYChart.Series series = new XYChart.Series("Пошук вибірки", FXCollections.observableArrayList());
                analyzeChart.getData().add(series);
                SampleSearchModule sampleSearchModule = new SampleSearchModule(SettingClass.start_cash, SettingClass.coef_c, lengthProperty.get(), countProperty.get(),
                        SettingClass.thread_count, iterateCountProperty.get(), modelingModule, series, epsProperty.get(), taskProgressView);
                sampleSearchModule.setFinishAction(() -> {
                    MonologGeneratorPane.showAcceptDialog("Пошук закінчився!", "Було знайдено:" + sampleSearchModule.getFindCount());
                    modelingModule.setShowMessage(true);
                    freeAnalyze = true;
                });
                sampleSearchModule.start();
            }
        }
    }

    @FXML
    private void settingAction(ActionEvent event) {
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("/setting.fxml"));
            stage.setScene(new Scene(loader.load()));
            SettingController controller = loader.getController();
            controller.init(stage);
            stage.initOwner(this.stage);
            stage.showAndWait();
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
    }

    @FXML
    private void runAction(ActionEvent event) {
        modelingModule.runModeling(taskProgressView, SettingClass.getRandom());
    }

    @FXML
    private void meanSearchAction(ActionEvent event) {
        AnjiDoubleProperty startCashProperty = new AnjiDoubleProperty("Початковий капітал", SettingClass.start_cash);
        if (DialogUtil.createDialog("Оберіть параметри", startCashProperty).showEdit()) {
            MeanSearchTask meanSearchTask = new MeanSearchTask(startCashProperty.get(), modelingModule, taskProgressView);
            taskProgressView.getTasks().add(meanSearchTask);
            meanSearchTask.setOnSucceeded(event1 -> {
                double result = (Double) event1.getSource().getValue();
                Log.log(Log.debugIndex, "Було все обчисленно:", result);
            });
            new Thread(meanSearchTask).start();
        }
    }

    @FXML
    private void testExponentialAction(ActionEvent event) {
        if (SettingClass.claimsDistribution == Distribution.EXPONENTIAL) {
            AnjiDoubleProperty cashProperty = new AnjiDoubleProperty("Початковий капітал", SettingClass.start_cash);
            if (DialogUtil.createDialog("Налаштування тесту", cashProperty).showEdit()) {
                MeanSearchTask meanSearchTask = new MeanSearchTask(modelingModule, taskProgressView);
                meanSearchTask.setStartCash(cashProperty.get());
                taskProgressView.getTasks().add(meanSearchTask);
                meanSearchTask.setOnSucceeded(event1 -> {
                    double result = (Double) event1.getSource().getValue();
                    MonologGeneratorPane.showInfoDialog("Все було обчислення", "Результат:" + result);
                    Log.log(Log.debugIndex, "Було все обчисленно:", result);
                });
                meanSearchTask.setFunction(new ExponentialNumericDoubleFunction(SettingClass.coef_c));
                new Thread(meanSearchTask).start();
            }
        } else {
            MonologGeneratorPane.showErrorDialog("Розподіл має бути експоненційним!");
        }
    }

    @FXML
    private void calculateFunction(ActionEvent event) {
        AnjiDoubleProperty startU = new AnjiDoubleProperty("Початок", SettingClass.start_cash / 2);
        AnjiDoubleProperty endU = new AnjiDoubleProperty("Кінець", SettingClass.start_cash);
        AnjiIntegerProperty saveCount = new AnjiIntegerProperty("Цикл збреження", 100);//магічна якась константа
        if (DialogUtil.createDialog("Оберіть межі обчислення функції", startU, endU, saveCount).showEdit()) {
            if (!(startU.get() < endU.get())) {
                MonologGeneratorPane.showErrorDialog("Неправильно вказані межі", "Початок має бути менше кінця");
            }
            FunctionCalculateTask task = new FunctionCalculateTask(startU.get(), endU.get(), saveCount.get(), modelingModule, taskProgressView);
            task.setOnSucceeded(event1 -> MonologGeneratorPane.showAcceptDialog("Обчислення закінчилися"));
            taskProgressView.getTasks().add(task);
            new Thread(task).start();
        }
    }

    @FXML
    private void meanModelingAction(ActionEvent actionEvent) {
        new Thread(() -> {
            Log.log(Log.debugIndex, "Запускаємо трошки моделювання");
            double result = modelingModule.blockMeanModeling(SettingClass.length, SettingClass.count, SettingClass.thread_count, SettingClass.start_cash, SettingClass.coef_c, taskProgressView, SettingClass.getRandom());
            Log.log(Log.debugIndex, result);
        }).start();
    }

    @FXML
    private void deVylderApproximationAction(ActionEvent actionEvent) {
        AnjiDoubleProperty startCash = new AnjiDoubleProperty("Початковий капітал", SettingClass.start_cash);
        if (DialogUtil.createDialog("Оберіть початковий капітал", startCash).showEdit()) {
            INumericFunction function = ExponentialNumericDoubleFunction.approximate();
            MeanSearchTask searchTask = new MeanSearchTask(modelingModule, taskProgressView);
            searchTask.setFunction(function);
            taskProgressView.getTasks().add(searchTask);
            searchTask.setOnSucceeded(event -> Log.log(Log.debugIndex, "Підрахунок завершився", event.getSource().getValue().toString()));
            new Thread(searchTask).start();
        }
    }

    private void runAdd(boolean addFunction, boolean addApproximation, boolean addModeling) {
        AnjiDoubleProperty startCash = new AnjiDoubleProperty("Початкове значення u", SettingClass.start_cash / 2),
                endCash = new AnjiDoubleProperty("Кінцеве значення u", SettingClass.start_cash);
        AnjiIntegerProperty stepCount = new AnjiIntegerProperty("Кількість точок", 10);
        if (DialogUtil.createDialog("Налаштовування порівняння", startCash, endCash, stepCount).showEdit()) {
            AddChartTask task = new AddChartTask(startCash.get(), endCash.get(), stepCount.get(),
                    compareChart, modelingModule, taskProgressView, addFunction, addApproximation, addModeling);
            taskProgressView.getTasks().add(task);
            new Thread(task).start();
        }
    }

    @FXML
    private void addFunctionAndApproximationAction(ActionEvent actionEvent) {
        runAdd(true, true, false);
    }


    @FXML
    private void addApproximationAction(ActionEvent actionEvent) {
        runAdd(false, true, false);
    }

    @FXML
    private void addFunctionAction(ActionEvent actionEvent) {
        runAdd(true, false, false);
    }

    @FXML
    private void addModelingAction(ActionEvent actionEvent) {
        runAdd(false, false, true);
    }

    @FXML
    private void compareChartClearAction(ActionEvent actionEvent) {
        if (MonologGeneratorPane.showQuestionDialog("Почистити графік порівнянь?", "Ніяких даних не буде збережено!")) {
            compareChart.getData().clear();
        }
    }

    @FXML
    private void errorFindAction(ActionEvent actionEvent) {
        if (compareChart.getData() == null || compareChart.getData().size() == 0)//Заглушка
            return;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("/selectSeries.fxml"));
        try {
            Scene scene = new Scene(loader.load());
            SelectSeriesController controller = loader.getController();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(this.stage);
            stage.setTitle("Оберіть серії для порівняння");
            controller.init(stage, compareChart);
            stage.setScene(scene);
            stage.showAndWait();
            ObservableList<XYChart.Series<Number, Number>> target = controller.getTarget();
            int size = target.size();
            DoubleMatrix errorMatrix = DoubleArrayMatrix.fixed(size, size);
            for (int i = 0; i < size; i++) {
                for (int j = i + 1; j < size; j++) {
                    ObservableList<XYChart.Data<Number, Number>> i_data = target.get(i).getData();
                    ObservableList<XYChart.Data<Number, Number>> j_data = target.get(j).getData();
                    int lenght = Math.min(i_data.size(), j_data.size());
                    double max_error = 0;
                    for (int k = 0; k < lenght; k++) {
                        double temp_error = Math.abs(i_data.get(k).getYValue().doubleValue() - j_data.get(k).getYValue().doubleValue());
                        if (max_error < temp_error) {
                            max_error = temp_error;
                        }
                    }
                    errorMatrix.set(i, j, max_error);
                    errorMatrix.set(j, i, max_error);
                }
            }
            List<String> nameList = target.stream().map(XYChart.Series::getName).collect(Collectors.toList());
            SwingNode node = AnjiFXMathConverter.convertMatrix(errorMatrix, nameList, nameList);
            MonologGeneratorPane.showNodeAcceptDialog("Матриця помилок", node);
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
    }

    @FXML
    private void viewSeriesAction(ActionEvent actionEvent) {
        if (compareChart.getData() == null || compareChart.getData().size() == 0)//Заглушка
            return;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("/selectSeries.fxml"));
        try {
            Scene scene = new Scene(loader.load());
            SelectSeriesController controller = loader.getController();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(this.stage);
            stage.setTitle("Оберіть серії для порівняння");
            controller.init(stage, compareChart);
            stage.setScene(scene);
            stage.showAndWait();
            ObservableList<XYChart.Series<Number, Number>> target = controller.getTarget();
            StringJoiner joiner = new StringJoiner("\n");
            target.forEach(s -> Log.log(Log.debugIndex, s.getName(), '\n', s.getData()));
            target.forEach(s -> joiner.add(s.toString()).add(s.getData().stream().map(data -> data.getYValue().toString()).collect(Collectors.joining(";"))));
            MonologGeneratorPane.showDetailAcceptDialog("Список серій", joiner.toString());
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
    }

    @FXML
    private void saveCompareAction(ActionEvent actionEvent) {
        WritableImage image = compareChart.snapshot(new SnapshotParameters(), null);
        FileChooser chooser = new FileChooser();
        File imageDirectory = new File("images");
        if (!Files.exists(imageDirectory.toPath())) {
            imageDirectory.mkdir();
        }
        chooser.setInitialDirectory(imageDirectory);
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Картинка", "*.png"));
        File f = chooser.showSaveDialog(stage);
        if (f != null) {
            if (!f.getAbsolutePath().contains(".png")) {
                f = new File(f.getAbsolutePath() + ".png");
            }
            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", f);
            } catch (IOException e) {
                Log.error(Main.Log_index, e);
                MonologGeneratorPane.showErrorDialog("Помилка збереження", e.getMessage());
            }
        }
    }
}
