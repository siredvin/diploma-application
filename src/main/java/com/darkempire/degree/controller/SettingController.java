package com.darkempire.degree.controller;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.monolog.MonologController;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.anjifx.scene.DoubleField;
import com.darkempire.anjifx.scene.IntegerField;
import com.darkempire.degree.Main;
import com.darkempire.degree.MeanCutMethod;
import com.darkempire.degree.SettingClass;
import com.darkempire.math.struct.probability.ExponentialDistribution;
import com.darkempire.math.struct.probability.IDistribution;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;


import static com.darkempire.degree.SettingClass.incomeLambda;
import static com.darkempire.degree.SettingClass.randomModule;

/**
 * Created by siredvin on 29.04.15.
 *
 * @author siredvin
 */
public class SettingController {
    @FXML
    private ComboBox<MeanCutMethod> cutMethodComboBox;
    @FXML
    private DoubleField uField;

    @FXML
    private DoubleField cField;

    @FXML
    private DoubleField incomeField;

    @FXML
    private IntegerField lengthField;

    @FXML
    private IntegerField countField;

    @FXML
    private IntegerField threadField;

    @FXML
    private Text randomTypeText;

    private Stage stage;

    @FXML
    void initialize() {
        incomeField.valueProperty().addListener((observable, oldValue, newValue) -> SettingClass.incomeLambda = newValue);
        incomeField.setEditable(false);//Деякий велосипед для блокування
        cutMethodComboBox.getItems().addAll(MeanCutMethod.values());
    }

    public void init(Stage stage) {
        this.stage = stage;
        loadSetting();
    }

    private void loadSetting() {
        SettingClass.loadSetting();
        countField.setValue(SettingClass.count);
        lengthField.setValue(SettingClass.length);
        threadField.setValue(SettingClass.thread_count);
        uField.setValue(SettingClass.start_cash);
        cField.setValue(SettingClass.coef_c);
        incomeField.setValue(SettingClass.incomeLambda);
        cutMethodComboBox.getSelectionModel().select(SettingClass.cutMethod);
        updateRandomTypeText();
    }

    private void saveSetting() {
        SettingClass.start_cash = uField.getValue();
        SettingClass.coef_c = cField.getValue();
        SettingClass.length = lengthField.getValue();
        SettingClass.count = countField.getValue();
        SettingClass.thread_count = threadField.getValue();
        SettingClass.incomeLambda = incomeField.getValue();
        SettingClass.incomeDistributionImpl = new ExponentialDistribution(incomeLambda);
        SettingClass.cutMethod = cutMethodComboBox.getSelectionModel().getSelectedItem();
        SettingClass.saveSetting();
    }


    private IDistribution changeDistribution(IDistribution distribution, String title) {
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/distribution.fxml"));
            Scene scene = new Scene(loader.load());
            DistributionStageController controller = loader.getController();
            stage.setScene(scene);
            controller.init(stage, distribution);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(this.stage);
            stage.setTitle(title);
            stage.showAndWait();
            if (controller.isEdited()) {
                return controller.getResult();
            }
        } catch (IOException e) {
            Log.error(Main.Log_index, e);
        }
        return null;
    }


    @FXML
    void acceptAction(ActionEvent event) {
        if (cField.getValue() <= 1) {
            MonologGeneratorPane.showErrorDialog("Неправильні агрументи", "Коефіцієнт C має бути більше одиниці");
        } else {
            saveSetting();
            stage.close();
        }
    }

    @FXML
    void cancelAction(ActionEvent event) {
        stage.close();
    }

    @FXML
    void claimsButtonAction(ActionEvent event) {
        IDistribution result = changeDistribution(SettingClass.claimsDistributionImpl, "Розподіл виплат");
        if (result != null) {
            SettingClass.claimsDistributionImpl = result;
            SettingClass.saveSetting();
        }
    }

    @FXML
    void randomTypeClickAction(MouseEvent event) {
        int size = randomModule.getSize();
        if (size == 0) {
            randomModule.searchRandomGenerators();
        } else {
            if (MonologGeneratorPane.showQuestionDialog("Змінити рандомізацію?", "Ви точно хочете повернутися до системної рандомізації?")) {
                randomModule.clear();
            } else {
                randomModule.searchRandomGenerators();
            }
        }
        updateRandomTypeText();
    }

    private void updateRandomTypeText() {
        int size = randomModule.getSize();
        if (size == 0) {
            randomTypeText.setText("системна");
            randomTypeText.setFill(Color.RED);
        } else {
            randomTypeText.setText("зовнішня");
            randomTypeText.setFill(Color.GREEN);
        }
    }
}
